#ifndef _TME1_H
#define _TME1_H
#include <sys/types.h>
#include <unistd.h>
#include "file.h"


// structure definissant un paquet contenant une chaines de caracateres 
typedef struct _paquet {
    char * nom;
}paquet;

typedef struct _compteur {
    int i ;
}compteur ;

// structure definissant un tapye contenant une capacité un compteur et  de consomations (pour les consomateur )
typedef struct _tapis {
    file *file;
    int cpt ;
    int capacity;
}tapis;

// structure definissant les  arguments de la fonction producteur des threads producteurs 
typedef struct _Producteur{
    int cpt;
    tapis * t;
    char * nom;
}Producteur;
//strucutre definissant les arguments de la fonction consomateur des threads consomateurs 
typedef struct _Consomateur{
    // int *cpt;
    tapis * t;
    char * nom;
}Consomateur;

// un mutex etune conditiosn d'attentes sur les threads 
pthread_mutex_t mutex;
pthread_cond_t condition;

// initialisation du mutex ainsi que la condition
pthread_mutex_init(mutex);
pthread_cond_init(condition);

// fonction qui prend les argument s de la fonction producteurs et renvoie un pointeurs generique 
Producteur * creation_Producteur(int cpt , tapis *t , char * nom){

    Producteur * a = (Producteur *)malloc(sizeof(Producteur));
    a->cpt=cpt;
    a->nom=nom;
    a->t=t;
    return a;
}

// fonction qui prend les arguments de la fonction consomateur et renvoie un pointeur generique 
Consomateur * creation_Consomateur( tapis *t , char * nom){

    Consomateur * a = (Consomateur *)malloc(sizeof(Consomateur));
    a->nom=nom;
    a->t=t;
    return a;
}

void desc(compteur * a ){
    pthread_mutex_lock(&mutex);
    if( a ->i >0){
        a->i--;
    }
    pthread_mutex_unlock(&mutex);
}
// fonction qui permet d'allouer la memeoire d'un paquet en prennant comme argument le nom du paquet 
paquet * creation_paquet(char * nom){
    
    paquet * p = (paquet *) malloc(sizeof(paquet));
    
    if( nom == NULL){
        p->nom ="";
        return p;
    }

    p->nom=nom;
    return p;
}

// fonction qui permet d'allouer la memoire pour un tapis prenant en argument sa capacité ainsi que le taux de consomation
tapis * creation_tapis(int capacity,int consomations ){

    if( capacity<1){
        return NULL;
    }

    tapis *t=(tapis *) malloc (sizeof(tapis));
    t->file=creation_file_vide();
    t->capacity=capacity;
    t->cpt=consomations-1;

    return t;

}

// fonction qui permet d'enfiler dans un tapis un paquet fonction executer par les produteurs 
void enfilerT(tapis * t,paquet *p){
    // recupere le verou 
    pthread_mutex_lock(&mutex);
    if( t==NULL || p == NULL){
        return;
    }

    // condition de blockage 
    while (t->capacity == t->file->nbelement){
        pthread_cond_wait(&condition,&mutex);
    }
    // enfiler le paquet en queue de file 
    enfiler(t->file,p->nom);
    // reveillez les autres threads bloquer 
    pthread_cond_broadcast(&condition);
    // lacher le verou 
    pthread_mutex_unlock(&mutex);
}

// fonction qui permet de defiler un paquet de la file du tapis 
void defilerT(tapis *t,char * nom){
    // recuperations du verou 
    pthread_mutex_lock(&mutex);
    if(t == NULL ){
        return;
    }
    //condition de blockage nombre d'elements à 0 
    while( t->file->nbelement == 0 ){
        // lacher le main 
        pthread_cond_wait(&condition,&mutex);
    } 
    // recuperation du paquet en tete de file 
    paquet *p =creation_paquet(defiler(t->file)->nom);
    // decrementation du compteur de consmoation 
    t->cpt--;
    // affichage de consomation 
    printf("%s mange %s  \n",nom,p->nom);
    // notifier les threads bloquer 
    pthread_cond_broadcast(&condition);
    // lacher le verou 
    pthread_mutex_unlock(&mutex);
}

// fonction definissant l'activite du producteur (thread )
void producteur (void * ar){
    Producteur * a =(Producteur *)ar;
    if( a->cpt  < 0 || a->nom ==NULL || a->t ==NULL ){
        return;
    }

    int i =0; 
    // executer une suite d'enfilemeent sur le meme tapis 
    for (i=0;i<a->cpt;i++){
    paquet * p =creation_paquet(a->nom);
    enfilerT(a->t,p);
    }
    
}

// fonction definissant le comportemement d'un consomateur 
void consomateur (void *ar){
    Consomateur * a =(Consomateur *)ar;
    // tant que le compteur n'ai pas a zero defiler des paquet du tapis 
    while (a->t->cpt>0){
        defilerT(a->t,a->nom);   
    }
}
#endif