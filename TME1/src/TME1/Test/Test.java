
package TME1.Test;
import TME1.Classes.Compteur;
import TME1.Classes.Tapis;
import TME1.Threads.Consomateurs;
import TME1.Threads.Producteurs;

public class Test {

	public static void main(String[] args) {
		// tapis 
		Tapis tapis =new Tapis(16);
		
		//liste de producteur 
		Producteurs[] producteurs=new Producteurs[2];
		//liste de consomateur 
		Consomateurs[] consomateurs=new Consomateurs[2];
		// fruit 
		String [] fruits={"Pomme", "Banane"};
		
		//compteur 
		Compteur cpt =new Compteur(8);
		
		// creation des producteur et des consomateurs 
		for (int i=0;i<2;i++ ) {
			producteurs[i]=new Producteurs(tapis, fruits[i], 4); 
			consomateurs[i]=new Consomateurs(tapis, "C"+i,cpt); 
		}
		
		// lacement des producteurs et des consomateurs 
		for (int i=0;i<consomateurs.length;i++) {
			consomateurs[i].start();
		}
		for (int i=0;i<producteurs.length;i++) {
			producteurs[i].start();
	
		}	
		
		// appeler les methodes join sur les consomateurs et producteurs pour attendre la fin de leurs executions avant de terminer le thread main 
		try {
			
			for (int i=0;i<consomateurs.length;i++) {
				consomateurs[i].join();
			}
			
			for (int i=0;i<producteurs.length;i++) {
				producteurs[i].join();	
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// message de fin de programme 
		System.out.println("fin du programme ");		
	}

}
