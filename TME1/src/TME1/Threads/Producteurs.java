package TME1.Threads;
/*
 * Binomes :
 * 	- KICHOU yanis 3703169	  
 *  - GALOU Arezki 3702608
 * 
 * 
 */
import TME1.Classes.Paquet;
import TME1.Classes.Tapis;

public class Producteurs extends Thread {

	// nbpquet represente le nombre de paquet que le producteur doit produire 
	private int nbPaquet;
	// nom du produit a produire 
	private String nom;
	// tapis qui contient les paquets que le producteur affecteura et ainsi les consomateurs pouront y beneficier 
	private Tapis tapis ;
	
	// constructeur permettant de  cree un tapis 
	public Producteurs(Tapis tapis ,String nom , int nbPaquet) {
		this.nom=nom;
		this.nbPaquet=nbPaquet;
		this.tapis=tapis;
	}
	

	
	// methode run qui serra appeler dans le start() du thread producteur 
	public void run() {
		// boucle permettant de forcer le thread a cree nbPaquet paquets 
		for (int i =0;i<nbPaquet;i++) {
			// appel a la methode bloquante enfiler de tapis qui permet d'enfiler un paquet 
			tapis.enfiler(new Paquet(nom+" "+i));
			
		}
	}
}
