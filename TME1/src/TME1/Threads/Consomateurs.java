package TME1.Threads;

import TME1.Classes.Compteur;
/*
 * Binomes :
 * 	- KICHOU yanis 3703169	  
 *  - GALOU Arezki 3702608
 * 
 * 
 */
import TME1.Classes.Tapis;

public class Consomateurs extends Thread {

	// cpt representant le nombre de paquet que le consomateur doit recuperer du tapis 
	private Compteur cpt;
	// nom du consomateur 
	private String nom;
	// tapis contenant une file de paquet que le consomateur peut recuperer 
	private Tapis tapis;
	

	// osntructeur qui permet de construire un consomateur 
	public Consomateurs(Tapis tapis,String nom,Compteur cpt) {
		this.cpt = cpt;
		this.nom=nom;
		this.tapis=tapis;
	}
	
	// methode run qui serra executer dans la methode start) du  thread 
	public void run() {
		
		// nombre de paquet a recupere 
		while(cpt.getCpt()>0) {
			
			System.out.println(nom + " mange "+tapis.defiler(cpt).getContenue());
				
				// affichage des inforamtion conteu dans le paquet et recuperation d'un paquet grace a la fonction defiler qui est bloquante dans le cas ou ya pas de pauet disponible 
				
		}
	}
}
