/*
 * Binomes :
 * 	- KICHOU yanis 3703169	  
 *  - GALOU Arezki 3702608
 * 
 * 
 */
package TME1.Classes;
public class Compteur {

	// compteur entier 
	private int cpt; 
	
	
	public Compteur( int cpt ) {
		this.cpt=cpt;

	}
	
	// recuperations de la valeur du compteur 
	public int getCpt() {
		return cpt;
	}
	
	// methode qui decremente le compteur 
	public synchronized void decrementer() {
		cpt--;
	}
}
