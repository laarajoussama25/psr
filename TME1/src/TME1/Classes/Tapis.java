/*
 * Binomes :
 * 	- KICHOU yanis 3703169	  
 *  - GALOU Arezki 3702608
 * 
 * 
 */
package TME1.Classes;

import java.util.ArrayList;
import java.util.List;

public class Tapis {

	// nBelement rempresentant le nombre d'elements contenue dans la file 
	private  int nbElement,capacity;
	
	// file stanquant les elements du tapis 
	private List<Paquet> file ;
	
	// mutex  
	private final Object mutex =new Object();

	
	// construteur 
	public Tapis(int length) {
		// initialisation de la file 
		file=new ArrayList<Paquet>();
		//nombre d'elemnt de la fille a -1 (indice du dernier element 
		nbElement=-1;
		capacity=length-1 ;
	}
	
	// accesseur sur l'indice du dernier element 
	public int getContenue() {
		return file.size()-1;
	}
	
	// methode enfiler permettant d'ajouterun paquet dans la file 
	public void enfiler(Paquet paquet) {
		// recuperation du verou 
	synchronized (mutex) {
		
			while (nbElement==capacity) {
				try {
					mutex.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// place libére incrementer l'indice d'ajout
			nbElement++;
			// ajouter le paquet a la file 
			file.add(paquet);
			mutex.notifyAll();
		}
	
	}
	
	
	// methode defiler qui permet de recupere le dernier element enfiler dans la file 
	public Paquet defiler(Compteur cpt ) {
		synchronized (mutex) {
			while (nbElement<0) {
				try {
					mutex.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			Paquet 	paquet=file.remove(0);
			nbElement--;
			cpt.decrementer();
			mutex.notifyAll();
			return paquet;
		}
	}
	
}
