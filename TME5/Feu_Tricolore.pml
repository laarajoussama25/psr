mtype { ROUGE ,ORANGE, VERT, ORANGEBLOCK IND }
chan observateur = [0] of { mtype ,bool }
active proctype feu(){
    bool clignotant = false; 
    mtype couleur = IND
    goto initial;
    initial : atomic{
        couleur = ORANGE
        clignotant = true
        observateur ! couleur , clignotant
    }
    if 
    :: true -> clignotant = false; goto gored 
    fi
    gored : atomic {
        couleur = ROUGE 
        observateur ! couleur , clignotant
    }
    if 
    :: true -> goto gogreen 
    :: true ->goto goorangeBlock
    fi
    gogreen :atomic {
        couleur = VERT 
        observateur ! couleur , clignotant
    }
    if 
    :: true ->goto initial
    :: true -> goto goorangeBlock
    fi
    
    goorangeBlock : atomic {
        couleur = ORANGEBLOCK 
        clignotant = true
        observateur ! couleur , clignotant
        goto fault
    }
    fault :
        false
    }

active proctype observteurs() {
    mtype dir ;
    bool bo ;
    do :: observateur?(dir,bo) ->
    if 
    :: dir == IND -> printf("go Sortie "); goto exit
    :: else -> printf("go %e\n",dir)
    fi
    od
    exit:
        false
}
