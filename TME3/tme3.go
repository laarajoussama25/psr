/*
   Binomes :
       KICHOU Yanis 3703169
       GALOU Arezki 3702608

*/

// probleme de temps parsing
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	s "strings"
	"time"
)

// structure de paquet encapsulant 3 champs d'arriver de depart et des nombre d'arrete
type paquet struct {
	arrivee string
	depart  string
	duree   int
	retour  chan paquetResultant
}

type paquetResultant struct {
	arrivee string
	depart  string
	duree   int
}

func caculateur(calculer chan paquet, f chan int) {

	for true {
		select {
		case <-f:
			break
		case req := <-calculer:
			go func(res paquet) {
				depart, _ := time.Parse("15:04:05", res.depart)
				retour, _ := time.Parse("15:04:05", res.arrivee)
				m := depart.Sub(retour)
				pResultat := paquetResultant{res.depart, res.arrivee, int(m.Nanoseconds())}
				res.retour <- pResultat
			}(req)
		}
	}
}

// lecteurs qui prend en paramettre une fichier et 3 cannaux de flux
// - lit une ligne du fichier
// - l'envoie au travailleur pour bosser desu
func lecteur(lect chan int, workers chan string, f *os.File, end chan int) {
	<-lect
	r4 := bufio.NewReader(f)
	b4, err := r4.ReadString('\n')
	for err != io.EOF {
		workers <- b4
		b4, err = r4.ReadString('\n')
	}
	end <- 0
}

func travailleur(work chan string, calcule chan paquet, reduct chan paquetResultant, f chan int) {

	for true {
		// reception du paquet
		valeur := <-work

		// recuperationd es champs interessant
		tableauChamp := s.Split(valeur, ",")

		retour := make(chan paquetResultant)
		// creation du paquet a envoyez

		p := paquet{tableauChamp[1], tableauChamp[2], 0, retour}

		//passer le paquet au calculateur via son canal
		calcule <- p

		select {
		//recuperation du resultat du traitement du paquet
		case resultat := <-retour:
			// envoie du paquet au reducteur pour son traitement
			reduct <- resultat
		case <-f:
			return
		}
	}
}
func reducteur(work chan paquetResultant, temps chan int) {
	resultat := 0
	for true {
		select {
		case <-temps:
			temps <- resultat
		default:
			p := <-work
			resultat = resultat + p.duree
		}
	}
}

func main() {
	fichier, err := os.Open("./export_gtfs_voyages/stop_times.txt")
	if err != nil {
		fmt.Printf("erreure ouverture fichier \n ")
	}
	// creation des cannaux de communication

	// lect : canal de communication avec le lecteur
	lect := make(chan int)

	//canal dedier au travaulleur
	workers := make(chan string, 10)

	// canal de communication avec le serveur
	calcule := make(chan paquet)

	//canal de communication avec le reducteur
	reduct := make(chan paquetResultant)

	// canal specialement dedier au reducteur pour avertir du fin du travail du reducteur
	fin := make(chan int)

	end := make(chan int)

	temps := make(chan int)
	// reste le calculateurs et les servants
	go reducteur(reduct, temps)

	// lancement des travailleur
	for j := 0; j < 5; j++ {
		go travailleur(workers, calcule, reduct, fin)
	}

	//lancement du calculateur
	go caculateur(calcule, fin)

	//lancement du lecteur
	go lecteur(lect, workers, fichier, end)

	// signal sur le canal du lecteur pour debuter son travail
	lect <- 0
	<-end
	// attendre le signal de fin pour arreter le main

	// demander a tout les thread de s'arreter
	for i := 0; i < 6; i++ {
		fin <- 0
	}
	temps <- 0
	resultat := <-temps

	fmt.Printf(" temps total estimer pour le trajet est de  %d seconds \n", int(resultat/1000000000))

}
