#ifndef _TME2_H
#define _TME2_H
#include <sys/types.h>
#include <unistd.h>
#include "fthread.h"
#include<pthread.h>
#include "file.h"


#define Nombre_producteur   4
#define   Nombre_consomation  4
// structure definissant un pquet contenant une chaine de caracteres 
int indexprod = -1,indexconso=-1;
ft_event_t full[4],empty[4],messagerprod,messagerconso;
//un tapis contenant une file qui stock les paquet produit par les producteurs et a consommer par les consomateur ainsi que la capacité du tapis 
typedef struct _tapis {
    file *file;
    int capacity;
}tapis;

typedef struct _compteur{
    int cpt ;
}compteur;

// fonction qui permet d'allouer la memoire pour un tapis prenant en argument sa capacité ainsi que le taux de consomation
tapis * creation_tapis(int capacity){

    if( capacity<1){
        return NULL;
    }

    tapis *t=(tapis *) malloc (sizeof(tapis));
    t->file=creation_file_vide();
    t->capacity=capacity;

    return t;

}
// creations d'un messager 
typedef struct _Messager{
    // scheduler des producteurs 
    ft_scheduler_t production ; 
    // scheduler des consomateurs 
    ft_scheduler_t consomations ;
    // tapis des producteur 
    tapis * prod;
    // tapis des consomateur 
    tapis * conso;
    // nombre de consomatiosn total 
    compteur *nombre_consomations ;
}Messager;

// producteur 
typedef struct _Producteur{
    // nombre de production 
    int cpt;
    // tapis de oroducttion 
    tapis * t;
    int num;
    // nom du producteur  
    char * nom;
    // unité  a produire 
    paquet *paquet;
}Producteur;

//strucutre definissant les arguments de la fonction consomateur des threads consomateurs 
typedef struct _Consomateur{
    tapis * t;
    compteur *cpt;
    char * nom;
    int num;
}Consomateur;

// fonction qui prend les argument s de la fonction producteurs et renvoie un pointeurs generique 
Producteur * creation_Producteur(int cpt ,int num, tapis *t , char * nom,paquet * p){
    // allocation memoire pour producteur 
    Producteur * a = (Producteur *)malloc(sizeof(Producteur));
    // compteur de production 
    a->cpt=cpt;
    // onm du producteur 
    a->nom=nom;
    a->num=num;
    // tapis de production 
    a->t=t;
    // unité de production 
    a->paquet=p;
    // retourner le producteur créer  et initilaiser 
    return a;
}

// fonction qui prend les arguments de la fonction consomateur et renvoie un pointeur generique 
Consomateur * creation_Consomateur(tapis *t , compteur * cpt,char * nom,int num){
    // allocation memoire pour consomateur  
    Consomateur * a = (Consomateur *)malloc(sizeof(Consomateur));
    // nom consomateur  
    a->nom=nom;
    a->num=num;
    // tapis de consomation 
    a->t=t;
    // nombre de consoamtion global des consomateur 
    a->cpt=cpt;
    return a;
}

Messager * creation_messager(ft_scheduler_t prod,ft_scheduler_t conso,tapis *t  ,tapis * c ,compteur * cpt){
    if(prod ==  NULL || conso == NULL || cpt < 0) return NULL;
    // allocation memoir pour le messager 
    Messager * m = (Messager *) malloc(sizeof(Messager));
    // scheduleur de production 
    m->production=prod;
    // scheduleur de consomation 
    m->consomations=conso;
    m->prod=t;
    m->conso=c;
    m->nombre_consomations=cpt;
    return m;
}

compteur * creation_compteur (int cpt ){
    compteur * c = (compteur *)malloc(sizeof(compteur));
    c->cpt=cpt;
    return c ;
}

// fonction qui permet d'enfiler dans un tapis un paquet fonction executer par les produteurs 
void enfilerT( tapis * t ,paquet  * p ,int num ){
    
    if( t==NULL || p == NULL){
        return;
    }
    // condition de blockage 
    while (t->capacity== t->file->nbelement){
        ft_thread_await(full[num]);
    }
    // enfiler le paquet en queue de file 
    enfiler(t->file,p);
}

void producteur_run (void * ar){
    Producteur * a =(Producteur *)ar;
    if( a->cpt  < 0 || a->nom ==NULL || a->t ==NULL ){
        return;
    }

    int i =0; 
    // executer une suite d'enfilemeent sur le meme tapis 
    for (i=0;i<a->cpt;i++){
        paquet * p =creation_paquet(a->nom);
        enfilerT(a->t,a->paquet,a->num);
        ft_thread_generate(messagerprod);
        printf("Producteur : %s depose  %s \n",a->nom,a->paquet->nom); 
        ft_thread_cooperate();
       
    }
  
    
}
paquet *  defilerT( tapis *t,int num){
   // recuperations du verou 
    if(t == NULL ){
        return NULL;
    }
    //condition de blockage nombre d'elements à 0 
    while( t->file->nbelement == 0 ){
        // lacher le main 
        printf("Consomateur : j'attend  qu'un paquet sois deposer \n \n");
        ft_thread_await(empty[num]);
    } 
    // recuperation du paquet en tete de file 
    return defiler(t->file)->nom;
    // affichage de consomation 
   
}


void consomateur_run (void * ar){
    Consomateur * a =(Consomateur *)ar;
    if( a->cpt  < 0 || a->nom ==NULL || a->t ==NULL ){
        return;
    }
    // executer une suite d'enfilemeent sur le meme tapis 
    while (a->cpt->cpt>0){
        paquet * p = defilerT(a->t,a->num);  
        a->cpt->cpt--;
        ft_thread_generate(messagerconso);
        printf("%s mange %s compteur %d   \n\n",a->nom,p->nom, a->cpt->cpt);
        ft_thread_cooperate();
    }
}
void Messager_run(void * arg){
    Messager * m = (Messager * )arg;
    while (m->nombre_consomations->cpt>0){
        ft_thread_link(m->production);
        while (m->prod->file->nbelement == 0){
            printf(" Messager : en attente d'un depot de paquet \n");
            ft_thread_await(messagerprod);
        }
        paquet * p = defiler(m->prod->file)->nom;
        printf(" Messager :   prend %s du tapis de production \n \n \n ",p->nom);
        ft_thread_unlink();
        indexprod=((indexprod+1) % Nombre_producteur);
        ft_thread_generate(full[indexprod]);

        ft_thread_link(m->consomations);
        while ( m->conso->file->nbelement == m->conso->capacity){
            printf(" tapis plein attente d'une consomation de paquet \n\n ");
            ft_thread_await(messagerconso);
        }
        enfiler(m->conso->file,p);
        printf(" messager  depose %s du tapis de consomation  \n\n",p->nom);
        ft_thread_unlink();
        indexconso= ((indexconso+1) % Nombre_consomation);
        ft_scheduler_broadcast(empty[indexconso]);
    }
}
void traceinstants_consomation (void * arg ) {
  compteur *   c = (compteur * ) arg;
  int cpt =0 ;
  while(c->cpt>0) {
    printf(">>>>>>>>>> Scheduler instant %d consomations  %d :\n", cpt++,c->cpt);
    
    /*
      on ralentit expres pour avoir le temps de voir le deroulement de
      chaque instant en affichage.
    */
    sleep(1);
    
    ft_thread_cooperate ();
  }
}
#endif
