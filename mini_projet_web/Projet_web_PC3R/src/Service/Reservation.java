package Service;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import outils.OutilSession;
import outils.OutilsVol;

/**
 * 
 * @author root
 *
 */
public class Reservation {

	public static JSONObject reserverVol(String clef, String vol) throws JSONException, SQLException {
		JSONObject reponse = new JSONObject();
		if (clef == null || vol == null) {
			/** ajout de l'erreur au message du JSON*/
			reponse.append("id", 0);
			reponse.append("erreur", 100);
			reponse.append("Cause", "arguments invalide ");
		}else {
			/** verification si la session est lancer */
			if (!OutilSession.existanceClef(clef)) {
				reponse.append("id", 0);
				reponse.append("erreur", 300);
				reponse.append("Cause"," vous n'etes pas connecter / recconnectez vous S'il vous plait ");
			}else {
				/** verification si la clef est valide et que y'a pas un temps d'inactivité de 30 mins */
				if (!OutilSession.validiteClef(clef)) {
					reponse.append("id", 0);
					reponse.append("erreur", 300);
					reponse.append("Cause"," temps de non activité depassant 30 mins reconnecter vous ");
				}else {
					/** recuperation du pseudo de l'tulisateur */
					String pseudo = OutilSession.recuperationPseudo(clef);
					
					if (!OutilsVol.testDisponibiliteVol(vol)) {
							reponse.append("id", 0);
							reponse.append("erreur", 400);
							reponse.append("Cause"," vol saturé plus de disponibilité ");
						}else {
							/**verifier si l'utilisateur a deja reserver une place dans ce vol*/
							if(OutilsVol.testExistanceReservation(pseudo, vol)) {
								reponse.append("id", 0);
								reponse.append("erreur", 400);
								reponse.append("Cause"," vous avez deja reserver une place dans ce vol verifier votre profile ");
							}else {
								
								/**ajout de la nouvelle reservation du client*/
								reponse.append("id", 1);
								reponse.append("Message", "reservations effectuer ");
								BaseDeDonnee.Reservation.ajouterNouveauReservration(pseudo, vol);
							}
						}
					}
				}
			}
		return reponse;
	}

	public static JSONObject annulerReservationVol(String clef, String vol) throws JSONException, SQLException {
		JSONObject reponse = new JSONObject();
		if (clef == null || vol == null) {
			/** ajout de l'erreur au message du JSON*/
			reponse.append("id", 0);
			reponse.append("erreur", 100);
			reponse.append("Cause", "arguments invalide ");
		}else {
			/** verification si la session est lancer */
			if (!OutilSession.existanceClef(clef)) {
				reponse.append("id", 0);
				reponse.append("erreur", 300);
				reponse.append("Cause"," vous n'etes pas connecter / recconnectez vous S'il vous plait ");
			}else {
				/** verification si la clef est valide et que y'a pas un temps d'inactivité de 30 mins */
				if (!OutilSession.validiteClef(clef)) {
					reponse.append("id", 0);
					reponse.append("erreur", 300);
					reponse.append("Cause"," temps de non activité depassant 30 mins reconnecter vous ");
				}else {
					/** recuperation du pseudo de l'tulisateur */
					String pseudo = OutilSession.recuperationPseudo(clef);
					
					/**verifiaction si le vol existe*/
					if (!OutilsVol.testExistanceReservation(pseudo, vol)) {
						reponse.append("id", 0);
						reponse.append("erreur", 400);
						reponse.append("Cause"," vol inexistant dans la base de donnée  ");
					}else {
								/**ajout de la nouvelle reservation du client*/
								reponse.append("id", 1);
								reponse.append("Message", "annulation effectuer ");
								BaseDeDonnee.Reservation.annulerReservration(pseudo, vol);
							}
						}
					}
							
			}
		return reponse;
	}
}
