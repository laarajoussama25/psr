package test_unitaire.base_de_donnee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.SQLException;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class TesAPI {
	
	static StringBuffer response=new StringBuffer();
	public static JSONObject  Methode1() throws IOException, JSONException {
		BufferedReader reader ;
		HttpURLConnection connexion ;
		URL url = new URL("http://api.aviationstack.com/v1/flights?access_key=f5462d24f58640a7b1d96fb1eed2982e&&flight_status=active");
		connexion = (HttpURLConnection) url.openConnection();
		
		connexion.getRequestMethod();
		connexion.setConnectTimeout(100000);
		connexion.setReadTimeout(100000);
		
		int status = connexion.getResponseCode();
		System.out.println(status);
		
		if (status > 299) {
			reader = new BufferedReader(new InputStreamReader(connexion.getErrorStream()));
			while (reader.ready()) {
				response.append(reader.readLine());
			}
			reader.close();
		}else {
			reader = new BufferedReader(new InputStreamReader(connexion.getInputStream()));
			while (reader.ready()) {
				response.append(reader.readLine());
			}
			
			reader.close();
		}
		JSONObject j= new JSONObject(response.toString());
		return j;
	}
	
	public static void Methode2() {
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create("http://api.aviationstack.com/v1/flights?access_key=f5462d24f58640a7b1d96fb1eed2982e")).build();
		client.sendAsync(request, HttpResponse.BodyHandlers.ofString()).thenApply(HttpResponse::body).thenAccept(TesAPI::parse).join();
	}
	
	public static JSONObject parse(String res) {
		try {
			JSONObject j =new JSONObject(res); 
			return j;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		return new JSONObject();
		}
	}
	
	public static  String parserDAte(String date) {
		String datesp = date.replace("+", "T");
		String [] spliteDate = datesp.split("T");
		return spliteDate[0]+" "+spliteDate[1];
	}
	public static void main(String[] args) throws IOException, JSONException {
	JSONObject obj=Methode1();
	JSONArray jarr=obj.getJSONArray("data");
	System.out.println(jarr.get(0)+"\n");
	System.out.println("nombre de vol dans l'api "+jarr.length());
	for(int i=0 ;i<jarr.length();i++) {
		obj= (JSONObject) jarr.get(i);
		JSONObject o = new JSONObject(obj.get("airline").toString());
		String companie = o.getString("name");
		 o = new JSONObject(obj.get("flight").toString());
		 String id_vol=o.getString("iata");
		 String date= obj.get("flight_date").toString();
		 o=new JSONObject(obj.get("arrival").toString());
		 String airport_arrive=o.getString("airport");
		 o=new JSONObject(obj.get("departure").toString());
		 String airport_depart=o.getString("airport");
		 String time_date=o.getString("estimated");
		 int nbplaces=(int) (90 + (Math.random() * (220 - 90)));
		System.out.println("companie \t"+companie+"\n");
		System.out.println("id_vol \t"+id_vol+"\n");
		System.out.println("date vol \t"+date+"\n");
		System.out.println("aeroport_arrive \t"+airport_arrive+"\n");
		System.out.println("aeroport_depart\t"+airport_depart+"\n");
		System.out.println("nbplaces  "+nbplaces);
		System.out.println("estimated \t"+time_date+"\n	");
//		try {
//			BaseDeDonnee.Vol.ajouterNouveauVol(id_vol,parserDAte(time_date) , airport_depart, airport_arrive, nbplaces, companie);
//			System.out.println("Ajout réussi");
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
		
	}

	}
	
}
