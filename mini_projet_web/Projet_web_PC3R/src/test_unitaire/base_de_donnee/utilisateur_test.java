package test_unitaire.base_de_donnee;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import BaseDeDonnee.Utilisateur;
import outils.OutilsUtilisateur;

public class utilisateur_test {

	String nom ="Van dame";
	String prenom = "Jean-Claude"; 
	String pseudo= "jcvd";
	String motDePasse = "double impact"; 
	String nouveauMotDePasse = "tangPo";
	String email =" jeanClaude.vanDame@gmail.com";
	String telephone = "0123456789";
	int age = 40;
	
	@Test
	public void test_creation_nouveau_utilisateur() throws SQLException {
		assertTrue(Utilisateur.ajouterNouveauUtilisateur(nom, prenom, pseudo, email,motDePasse, age,telephone));
		assertTrue(OutilsUtilisateur.testExistanceUtilisateur(pseudo));
		assertTrue(OutilsUtilisateur.verificationMotDePasse(pseudo, motDePasse));
	}

	@Test
	public void test_modification_mot_de_passe_utilisateur() throws SQLException {
		assertTrue(Utilisateur.modificationModeDePasseUtilisateur(pseudo, nouveauMotDePasse));
		assertTrue(OutilsUtilisateur.verificationMotDePasse(pseudo, nouveauMotDePasse));
	}
	
	@Test
	public void test_suppression_utilisateur()throws SQLException{
		assertTrue(Utilisateur.supprimerUtilisateur(pseudo));
		assertFalse(OutilsUtilisateur.testExistanceUtilisateur(pseudo));
	}
	
}

