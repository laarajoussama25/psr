package test_unitaire.base_de_donnee;

import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.junit.Test;

public class Reservation_test {

	String pseudo = "kiki";
	String vol = "9NCAD";


	@Test
	public void reserverVol() throws SQLException {
		assertTrue(BaseDeDonnee.Reservation.ajouterNouveauReservration(pseudo, vol));
	}
	
	@Test
	public void annulerReservationVol() throws SQLException {
		assertTrue(BaseDeDonnee.Reservation.annulerReservration(pseudo, vol));
	}
}
