package test_unitaire.services;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import Service.Session;

public class Session_test {
	
	String pseudo = "jcvd";
	String motDepasse = "tangoPo";
	@Test
	public void demmarerSession() throws SQLException, JSONException {
		JSONObject reponse = Session.demarrerSession(pseudo,motDepasse);
		
		System.out.println("Message : "+reponse.getString("Message"));
		System.out.println("Clef :"+reponse.getString("Clef"));
	
	}
}
