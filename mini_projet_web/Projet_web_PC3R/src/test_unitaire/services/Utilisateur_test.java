package test_unitaire.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import outils.OutilsUtilisateur;

public class Utilisateur_test {

	static String clef;
	String pseudo ="jcvd";
	String nom = "van dame ";
	String prenom = "jean-claude";
	String motDePasse = "KickBoxer";
	String email = "jeanClaude.vanDame@gmail.com";
	String telephone = "0123456789";
	int age = 40;
	
	/**
	 * <p>
	 * <h1>Test 1 </h1> :
	 * 	 -Cree un utilisateur et test l'existance de cette utilisateur dans le base de donnée 
	 *	- verifie que le champs du mot de passe est bien enregistrer
	 * </p>
	 * @throws JSONException
	 * @throws SQLException
	 */
	@Test
	public void Utilisateur () throws JSONException, SQLException {
		/**appel au service  de creation d'utilisateur **/
		JSONObject reponse = Service.Utilisateur.ajouterUtilisateur(nom, prenom, pseudo, email, motDePasse, age, telephone);
		
		/**affichage du message retourner par le service  */
		System.out.println(reponse.getString("Message").toString());
		
		clef = reponse.getString("Clef");
		
		/** afficahge de la clef de session generer pour le compte */
		System.out.println("clef de session pour utilisateur "+ pseudo +" : "+ clef);
		
		/** test d'existance de l'utilisateur */
		assertTrue(OutilsUtilisateur.testExistanceUtilisateur(pseudo));
		
		/** test de la coeherence du mot de passe */
		assertTrue(OutilsUtilisateur.verificationMotDePasse(pseudo, motDePasse));
	
		/**nouveau mot de passe */
		String nouveauMotDePasse = "tanPo";
		
		System.out.println("deuxieme test "+clef.toString()+"\n "+reponse);
		
		/** appel au ervice de changement de mot de passe  */
		reponse = Service.Utilisateur.modifierMotDePasseUtilisateur(clef,motDePasse, nouveauMotDePasse);
		
		System.out.println("apres modification "+reponse);
		
		/**test de validité du changement */
		assertTrue(OutilsUtilisateur.verificationMotDePasse(pseudo, nouveauMotDePasse));

		/**appel au service de suppression d'un compte de l'utilisateur */
		reponse = Service.Utilisateur.supprimerUtilisateur(clef);
	
		/**affichage du message retourner par le service */
		System.out.println(reponse.getString("Message"));
			
		/** test de l'existance de l'utilisateur */
		assertFalse (OutilsUtilisateur.testExistanceUtilisateur(pseudo));
	}
}
